import styled from 'styled-components';
import { color, opacity } from '../../styles';

export default styled.div`
  display: inline-flex;
  transition: background 300ms ease;
  &:hover {
    background: ${color.primaryColor}${opacity.bg};
    cursor: pointer;
  }
  &[role=like]:hover {
    background: ${color.likeColor}${opacity.bg};
  }
`;