import React, { MouseEvent } from 'react';
import styled from 'styled-components';
import Button from '../Button';
import { TweetType } from '../../types';
import Like from '../Icons/Like';

export interface TweetElementProps {
  tweet: TweetType;
  className?: string;
  handleLike?: (event: MouseEvent) => void;
  liked?: boolean;
}

const TweetElement: React.FC<TweetElementProps> = ({
  tweet,
  className,
  handleLike,
  liked,
}) => {
  const { account, content, timestamp } = tweet;
  // calculate the passed time
  // I would usually use packages like moment js for time displaying
  const passedSeconds: number = Math.round((Date.now() - timestamp) / 1000);
  return (
    <div className={`tweet-element ${className}`}>
      <div className="info">
        <div className="account">{account}</div>
        {passedSeconds > 0 && (
          <>
            <div className="divider">.</div>
            <div className="time">{passedSeconds}s</div>
          </>
        )}
      </div>
      <div className="content">{content}</div>
      <Button
        onClick={handleLike}
        role="like"
        className="like-button"
        title={liked ? 'unlike' : 'like'}
      >
        <Like liked={liked} />
      </Button>
    </div>
  );
};

export default styled(TweetElement)`
  display: flex;
  flex-direction: row;
  flex-wrap: wrap;
  .info {
    width: 100%;
    margin-bottom: 10px;
    & > * {
      display: inline-flex;
    }
    .account {
      color: ${({ theme }) => theme.primaryFontColor};
      font-weight: bold;
      margin-right: 10px;
    }
    .divider {
      position: relative;
      bottom: 15%;
      margin-right: 10px;
    }
  }
  .content {
    display: inline-flex;
    flex: 1;
  }
  .like-button {
    flex: 0 0 auto;
    display: inline-flex;
    padding: 5px;
    height: 25px;
    width: 25px;
    border-radius: 50%;
    display: flex;
    justify-content: center;
    align-items: center;
    margin-left: auto;
  }
`;