import React, { useCallback, MouseEvent, useContext } from 'react';
import { TweetType } from '../../types';
import TweetLikeContext from '../../context/TweetLike';
import TweetElement from './Element';

export interface TweetElementProps {
  tweet: TweetType;
  className?: string;
}

const ConnectedTweetElement: React.FC<TweetElementProps> = ({ tweet, className }) => {
  const { isLiked, toggleLike } = useContext(TweetLikeContext);
  const liked = isLiked(tweet);
  const handleLike = useCallback((event: MouseEvent): void => {
    toggleLike(tweet);
  }, [tweet, toggleLike]);
  return <TweetElement tweet={tweet} liked={liked} handleLike={handleLike} className={className} />;
};

export default ConnectedTweetElement;


