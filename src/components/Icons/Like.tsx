import React from 'react';
import styled from 'styled-components';
import { AiFillHeart, AiOutlineHeart } from 'react-icons/ai';
import { color } from '../../styles';


interface LikeProps {
  liked?: boolean;
  className?: string;
}

const Like: React.FC<LikeProps> = ({ className, liked }) =>
  <div className={`like-icon ${className} ${liked ? 'liked' : ''}`}>
    {liked ? <AiFillHeart /> : <AiOutlineHeart />}
  </div>;

export default styled(Like)`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  transition: color 0.2s ease-in;
  svg {
    fill: ${color.secondaryFontColor} !important;
  }
  &.liked, &:hover {
    svg {
      fill: ${color.likeColor} !important;
    }
  }
`;