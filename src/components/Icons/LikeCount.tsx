import React from 'react';
import styled from 'styled-components';
import { AiOutlineNumber, AiOutlineHeart } from 'react-icons/ai';
import { color } from '../../styles';


interface LikeProps {
  className?: string;
  title?: string;
}

const Like: React.FC<LikeProps> = ({ className, title }) =>
  <div className={`like-count-icon ${className}`} title={title}>
    <AiOutlineHeart /> <AiOutlineNumber />
  </div>;

export default styled(Like)`
  display: inline-block;
  svg {
    fill: ${color.secondaryFontColor} !important;
  }
`;
