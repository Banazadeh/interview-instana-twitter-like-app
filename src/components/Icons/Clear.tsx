import React from 'react';
import styled from 'styled-components';
import { GrClear } from 'react-icons/gr';
import { color } from '../../styles';


interface ClearProps {
  className?: string;
}

const Clear: React.FC<ClearProps> = ({ className }) =>
  <div className={`clear-icon ${className}`}>
    <GrClear />
  </div>;

export default styled(Clear)`
  display: inline-flex;
  justify-content: center;
  align-items: center;
  svg {
    path {
      stroke: ${color.secondaryFontColor} !important;
    }
  }
`;