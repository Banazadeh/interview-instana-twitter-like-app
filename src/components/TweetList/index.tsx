import React from 'react';
import styled from 'styled-components';
import { color } from '../../styles';
import { TweetType } from '../../types';

// why not just import the types of the props of TweetElement here?
// because we want to keep TweetList possibly independent of TweetElement
interface ItemComponentProps {
  tweet: TweetType;
  liked?: boolean;
  className?: string;
}
interface TweetListProps {
  ItemComponent?: React.FC<ItemComponentProps>;
  items: TweetType[];
  className?: string;
}

const TweetList: React.FC<TweetListProps> = ({
  ItemComponent = item => <div>{item}</div>,
  items = [],
  className,
}) => (
  <div className={`tweet-list ${className}`}>
    {items.map(({ liked, ...tweet }) => <ItemComponent
      className="tweet-item"
      key={tweet.timestamp}
      tweet={tweet}
      liked={liked}
    />)}
  </div>
);

const StyledTweetList = styled(TweetList)`
  border-top: 1px solid ${color.borderColor};
  border-right: 1px solid ${color.borderColor};
  border-left: 1px solid ${color.borderColor};
  .tweet-item {
    // padding for the individual tweet
    padding: ${({ theme }) => `${theme.smallSpacing} ${theme.mediumSpacing}`};
    border-bottom: 1px solid ${color.borderColor};
  }
`;

// memorizing the TweetList so we only have to rerender if something really changes
export default React.memo(StyledTweetList);
