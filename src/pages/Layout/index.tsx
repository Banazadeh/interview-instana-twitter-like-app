import React from 'react';
import styled from 'styled-components';
import { mediaQ, size } from '../../styles';

type LayoutProps = {
  className?: string;
};

// adding possible layouts for navigation etc
const Layout: React.FC<LayoutProps> = ({ children, className }) => (
  <div className={className}>
    <div className="content">
      {children}
    </div>
  </div>);

export default styled(Layout)`
  height: 100vh;
  width: 100vw;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  .content {
    flex: 1;
    ${mediaQ.desktop} {
      margin: auto;
      width: ${size.md}px;
    }
  }
`;