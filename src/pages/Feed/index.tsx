import React, {
  useContext,
  useMemo,
  useState,
  useCallback,
  MouseEvent,
} from 'react';
import styled from 'styled-components';
import useTweets from '../../hooks/useTweets';
import TweetLikeContext from '../../context/TweetLike';
import TweetList from '../../components/TweetList';
import TweetElement from '../../components/TweetElement';
import Button from '../../components/Button';
import { TweetType } from '../../types';
import LikeCount from '../../components/Icons/LikeCount';
import Clear from '../../components/Icons/Clear';
import { mediaQ } from '../../styles';

// could move to a place to centralize such business logic
const threshold: number = 30 * 1000; // 30 seconds
// checks whether the the tweet is older than the threshold time
const filterOldTweets = ({ timestamp }: TweetType): boolean => Date.now() - timestamp  < threshold;
// sorts the tweets by date
// the tweets should be sorted by nature (see TweetsSubscription)
// thus this sort is of symbolic nature
const sortTweetsByDateDesc = (
  { timestamp: t1 }: TweetType,
  { timestamp: t2 }: TweetType,
): number => t2 - t1;

type FeedProps = {
  className?: string;
};

const Feed: React.FC<FeedProps> = ({ className }) => {
  // filter for liked tweets
  const [filterForLiked, setFilterForLiked] = useState<boolean>(false);
  const { tweets, clearTweets } = useTweets();
  const { isLiked, clearLikedMap } = useContext(TweetLikeContext);

  const reducedTweets: TweetType[] = useMemo(() => tweets
    .filter(filterOldTweets)
    // either filterForLiked is false and every tweet is shown
    // or the tweet should only be shown if the tweet is liked
    .filter((tweet: TweetType) => !filterForLiked || isLiked(tweet))
    .sort(sortTweetsByDateDesc),
  [tweets, filterForLiked, isLiked]);

  const likedCount: number = reducedTweets.reduce(
    // add 1 for each tweet which is "liked"
    // because of the nature of our likedMap we could
    // just count all set fields but we only want to show
    // the number of tweets which are not older than 30 seconds
    // thus we reduce reducedTweets
    (sum: number, tweet: TweetType) => sum + (isLiked(tweet) ? 1 : 0),
    0,
  );

  const handleShowAllTweets = useCallback(
    (event: MouseEvent): void => {
      setFilterForLiked(false);
    }, [setFilterForLiked],
  );
  const handleShowLikedTweets = useCallback(
    (event: MouseEvent): void => {
      setFilterForLiked(true);
    }, [setFilterForLiked],
  );
  const handleClearTweets = useCallback(
    (event: MouseEvent): void => {
      // eslint-disable-next-line
      const yes = window.confirm('Are you sure you want to delete all tweets?');
      if (yes) {
        clearTweets();
        clearLikedMap();
      }
      
    }, [clearTweets, clearLikedMap],
  );

  return (
    <div className={`feed ${className}`}>
      <div className="feed-navigation">
        <div className="selection">
          <Button onClick={handleShowAllTweets} className={`show-all ${filterForLiked ? '' : 'active'}`}>
            All Tweets
          </Button>
          <Button onClick={handleShowLikedTweets} className={`show-liked ${filterForLiked ? 'active' : ''}`}>
            Liked Tweets
          </Button>
        </div>
        <div className="misc">
          <div className="like-count" title="number of likes">
            <LikeCount /> <span className="counter">{likedCount}</span>
          </div>
          <Button onClick={handleClearTweets} className="clear-tweets" title="clear tweets">
            <Clear />
          </Button>
        </div>
      </div>
      <TweetList
        ItemComponent={TweetElement}
        items={reducedTweets}
        className="tweet-list"
      />
    </div>
  );
};

export default styled(Feed)`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  .feed-navigation {
    display: flex;
    align-items: center;
    padding: ${({ theme }) => `${theme.smallSpacing} ${theme.mediumSpacing}`};
    ${mediaQ.desktop} {
      padding-left: 0;
      padding-right: 0;
    }
    .selection {
      flex: 1;
      & > * {
        //margin-right: ${({ theme }) => theme.mediumSpacing};
        font-weight: bold;
        padding: ${({ theme }) => `${theme.smallSpacing} ${theme.mediumSpacing}`};
        &:hover {
          color: ${({ theme }) => theme.primaryAnchorColor}
        }
        &.active {
          border-bottom: 2px ${({ theme }) => `${theme.primaryColor}`} solid;
        }
      }
    }
    .misc {
      flex: 0 0 auto;
      display: flex;
      flex-direction: column;
      align-items: flex-end;
      .like-count {
        .counter {
          position: relative;
          bottom: 2px;
        }
      }
      .clear-tweets {
        border-radius: 50%;
        padding: 2px;
      }
    }
  }
  .tweet-list {
    flex: 1;
  }
`;
