// rxjs is exposed by
// https://cdnjs.cloudflare.com/ajax/libs/rxjs/6.4.0/rxjs.umd.min.js
import { interval, merge, Subscription as SubscriptionRxjs, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TweetType } from '../../types';

type TweetSubscriber = (newTweet: TweetType) => void;

// if we add additional Subscription Services we can move this and implement this interface properly
// or switch to class or abstract class depending on the features needed
export interface Subscription {
  Subscription: SubscriptionRxjs | null;
  subscribe: (subscriber: TweetSubscriber) => void;
  unsubscribe: () => void;
}

function createTweetSource(
  frequency: number, account: string, attribute: string): Observable<TweetType> {
  return interval(frequency)
    .pipe(map(i => ({
      account,
      timestamp: Date.now(),
      content: `${attribute} Tweet number ${i + 1}`,
    })));
}

const tweets = merge(
  createTweetSource(5000, 'AwardsDarwin', 'Facepalm'),
  createTweetSource(3000, 'iamdevloper', 'Expert'),
  createTweetSource(5000, 'CommitStrip', 'Funny'),
);

class TweetSubscription implements Subscription {
  constructor(subscriber: TweetSubscriber) {
    this.subscribe(subscriber);
  }

  Subscription = new SubscriptionRxjs();

  subscribe = (subscriber: TweetSubscriber) => {
    this.Subscription = tweets.subscribe(subscriber);
  };

  unsubscribe = () => this.Subscription.unsubscribe();
}

export default TweetSubscription;