import { createContext } from 'react';
import { TweetType } from '../types';

export type LikedMapType = {
  [key: string]: boolean;
};
export interface TweetLikeInterface {
  likedMap: LikedMapType;
  clearLikedMap: () => void;
  toggleLike: (tweet: TweetType) => void;
  isLiked: (tweet: TweetType) => boolean;
}

type GetKeyType = (tweet: TweetType) => string;

export const getKey: GetKeyType = ({ timestamp, account }: TweetType) => `${timestamp}_${account}`;

const initialTweetLikeContext: TweetLikeInterface = {
  likedMap: {},
  clearLikedMap: () => {},
  toggleLike: (tweet: TweetType) => {},
  isLiked: (tweet: TweetType) => false,
};

/**
 * We are using the React context api to save the like-state of each tweet.
 * In order to do that we will manage a liked map inside the context in 
 * which we track for each tweet whether it is liked or not.
 * To identify the tweet we create a key which consists of the timestamp and the account name.
 * This is definitely unique in our case because no use will be able to tweet
 * no more than 1 tweet at the same time.
 * 
 * We could instead of using React context use a global store via Redux
 * (usually with Redux-Thunk or Redux-Saga as a middleware).
 * That is usually the tool I use for global state management especially because
 * the context API is not fit for high frequency updates of global state.
 * But in this case it suites our needs pretty well and is pretty straight forward to use.
 */
const TweetLikeContext = createContext(initialTweetLikeContext);

export const { Consumer, Provider } = TweetLikeContext;

export default TweetLikeContext;
