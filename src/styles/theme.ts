import { DefaultTheme } from 'styled-components';

declare module 'styled-components' {
  export interface DefaultTheme {
    primaryColor: string;
    secondaryColor: string;
    primaryFontColor: string;
    secondaryFontColor: string;
    primaryAnchorColor: string;
    mediumSpacing: string;
    smallSpacing: string;
  }
}

export const mainTheme: DefaultTheme = {
  // light blue
  primaryColor: '#1DA1F2',
  // dark(er) blue
  secondaryColor: '#1A91DA',
  // dark grey front
  primaryFontColor: '#0f1419',
  // light grey font
  secondaryFontColor: '#5b7083',
  // light blue font
  primaryAnchorColor: '#1B95E0',
  mediumSpacing: '15px',
  smallSpacing: '10px',
};