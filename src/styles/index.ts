import { mainTheme } from './theme';

export const color = {
  ...mainTheme,
  borderColor: '#ebeef0',
  likeColor: '#E0245E',
};

export const opacity = {
  // background opacity
  bg: '1A', // hexa alpha value
};

// we will use the breakpoints of bootstrap
export const size = {
  sm: 576,
  md: 720,
  lg: 992,
  xl: 1200,
};

export const mediaQ = {
  mobileOnly: `@media only screen and (max-width: ${size.md - 1}px)`,
  desktop: `@media only screen and (min-width: ${size.md}px)`,
};
