import React, { useState } from 'react';
import styled, { ThemeProvider } from 'styled-components';
import { mainTheme } from './styles/theme';
import Layout from './pages/Layout';
import FeedPage from './pages/Feed';
import { Provider as TweetLikeProvider, TweetLikeInterface, getKey } from './context/TweetLike';
import { TweetType } from './types';

const App: React.FC = () => {
  // we create the state regarding the TweetLike context
  // and define it's 
  const [TweetLike, setTweetLike] = useState<TweetLikeInterface>({
    // we could of course use also a Map
    likedMap: {
    },
    clearLikedMap(): void {
      // reset likes
      TweetLike.likedMap = {};
      setTweetLike({ ...TweetLike });
    },
    // toggle the like state of a tweet
    toggleLike(tweet: TweetType): void {
      // create the key for the tweet
      const key = getKey(tweet);
      if (TweetLike.likedMap[key]) {
        // if already liked, remove the key for
        // this tweet from the likedMap
        delete TweetLike.likedMap[key];
      } else {
        // if not liked already, then add the key
        // for the tweet to the likedMap
        TweetLike.likedMap[key] = true;
      }
      // alter reference to trigger rerender of the consumers
      setTweetLike({ ...TweetLike });
    },
    // check if tweet is liked or not
    isLiked(tweet: TweetType): boolean {
      // create key
      const key = getKey(tweet);
      return Boolean(TweetLike.likedMap[key]);
    },
  });

  return (
    <ThemeProvider theme={mainTheme}>
      <TweetLikeProvider value={TweetLike}>
        <Layout className="twitter-like-app">
          <FeedPage />
        </Layout>
      </TweetLikeProvider>
    </ThemeProvider>
  );
};
    
export default styled(App)`
  .App {
    text-align: center;
  }

  .App-logo {
    height: 40vmin;
    pointer-events: none;
  }
  
  @media (prefers-reduced-motion: no-preference) {
    .App-logo {
      animation: App-logo-spin infinite 20s linear;
    }
  }
  
  .App-header {
    background-color: #282c34;
    min-height: 100vh;
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
    font-size: calc(10px + 2vmin);
    color: white;
  }
  
  .App-link {
    color: #61dafb;
  }
  
  @keyframes App-logo-spin {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
`;
