import { useEffect, useRef, useState } from 'react';
import { TweetSubscription } from '../../services/subscriptions';
import { TweetType } from '../../types'; 

interface UseTweetsInterface {
  tweets: TweetType[];
  clearTweets: () => void;
}

/**
 * establishes a subscription to the TweetsObservable
 * returns array of tweets already published
 * @param initialTweets
 */
const useTweets = (initialTweets: TweetType[] = []): UseTweetsInterface => {
  const [tweets, setTweets] = useState<TweetType[]>(initialTweets);

  const SubscriptionRef = useRef<TweetSubscription | null>(null);

  // wait for after the mount before subscription
  useEffect(() => {
    // subscribe to the tweets
    SubscriptionRef.current = new TweetSubscription((newTweet: TweetType) => {
      // push a new tweet into the state
      setTweets((oldTweets: TweetType[]) =>
        // do not Tweets.push(newTweet) or React.memo breaks
        // because the references of the array does not change.
        // React.memo is used in TweetList for example.
        [...oldTweets, newTweet],
      );
    });

    return () => {
      // unsubscribe when the component is unmounting
      SubscriptionRef.current?.unsubscribe();
    };
  },
  [SubscriptionRef, setTweets]);

  return {
    tweets,
    clearTweets: (): void => {
      setTweets([]);
    },
  };
};

export default useTweets;
